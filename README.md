# Bongo Code!

The three given problems were solved and tested on Python 3.5 .

# Problem 1
Problem 1 appears to be a dynamic programming problem and it is solved through a recursion method. It prints out the key and its depth as it goes through the dictionary. For testing purpose the output is also stored inside an output property of the **DictPrinter** class.

#### Execution
> python3 problem1.py
#### Running Test
> python3 test-problem1.py

# Problem 2
Problem 2 basically adds requirements on top of Problem 1 to recognize and traverse through Person objects as well. To implement this **DictPrinter** class is extended onto **MixedPrinter** class.

#### Execution
> python3 problem2.py
#### Running Test
> python3 test-problem2.py

# Problem 3
Problem 3 solved through another recursion method as well. Since the requirement was to pass 2 nodes to the method and then find the least common ancestor the recursion method back tracked the passed objects and stored two individual lists to identify intersection point which should be the least common ancestor, except edge cases. Edge cases are managed separately.  

#### Execution
> python3 problem3.py
#### Running Test
> python3 test-problem3.py