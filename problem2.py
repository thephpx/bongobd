from classes.Person import Person
from classes.MixedPrinter import MixedPrinter

person_a = Person("User", "1", None)
person_b = Person("User", "2", person_a)

b = {
  "key1": 1,
  "key2": {
    "key3": 1,
    "key4": {
      "key5": 4,
      "user": person_b,
    }
  },
}

MixedPrinter(b)