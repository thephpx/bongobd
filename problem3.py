from classes.Node import Node
from classes.Lca import LCA;

node1 = Node(1,None)
node2 = Node(2,node1)
node3 = Node(3,node1)
node4 = Node(4,node2)
node5 = Node(5,node2)
node6 = Node(6,node3)
node7 = Node(7,node3)
node8 = Node(8,node4)
node9 = Node(9,node4)

node12 = None
node13 = Node(13,None)

data = LCA(node12,node6)

print(data.lca)