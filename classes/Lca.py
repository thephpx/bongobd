from .Node import Node

class LCA(object):
  left = []
  right = []
  lca = None

  def __init__(self, a, b):
    self.left = []
    self.right = []
    self.lca = None
    self.LCA(a,b)
  
  def intersection(self, left, right): 
    inter = [value for value in left if value in right] 
    
    return inter

  def LCA(self, a, b):
    if(a == None or b == None):
      self.lca = None
      return self

    intersect = self.intersection(self.left, self.right)
    
    if(len(intersect) > 0 and type(intersect) == list):
      self.lca = int(intersect[0])     
      
      return self
    else:
      if(type(a) == Node):
        self.left.append(a.value)
      if(type(b) == Node):
        self.right.append(b.value)
    
    if(type(a.parent) == Node and type(b.parent) == Node):
      self.LCA(a.parent,b.parent)
    else:
      if(a.parent == None):
        self.lca = [a.value]
        return self
      if(b.parent == None):
        self.lca = b.value
        return self