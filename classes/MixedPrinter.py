from .DictPrinter import DictPrinter
from .Person import Person

class MixedPrinter(DictPrinter):
  
  def print_depth(self, currentlevel, data):
    if(data is None or data == ''):
      return '';
    for key,val in data.items():
      self.output.append(str(key)+' '+str(currentlevel)+'\n');
      print(str(key)+' '+str(currentlevel)+'\n')
      if(type(val) == dict):
        self.print_depth((currentlevel+1), val)
      if(type(val) == Person):
        self.print_depth((currentlevel+1), val.__dict__)
  