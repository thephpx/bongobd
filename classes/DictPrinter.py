class DictPrinter(object):
  data = {}
  output = []

  def __init__(self, data):
    self.data = data
    self.output = []
    self.print_depth(1,self.data)
  
  def print_depth(self, currentlevel, data):
    if(data is None or data == ''):
      return '';
    for key,val in data.items():
      self.output.append(str(key)+' '+str(currentlevel)+'\n');
      print(str(key)+' '+str(currentlevel)+'\n')
      if(type(val) == dict):
        self.print_depth((currentlevel+1), val)