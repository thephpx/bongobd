import unittest
from classes.MixedPrinter import MixedPrinter
from classes.Person import Person

class TestMixedPrinter(unittest.TestCase):

  def setUp(self):
    self.a = '';
    self.b = None;
    self.c = {'a':1,'b':2,'c':3};
    self.d = {'a':1,'b':{'c':3,'d':4,'e':5},'f':6,'g':{'h':7}};
    person_a = Person('User',1, None)
    self.e = {'a':1,'b':{'c':3,'d':4,'e':5},'f':6,'g':{'h':7,'i':person_a}};
    person_b = Person('User',2, None)
    person_c = Person('User',2, person_b)
    self.f = {'a':1,'b':{'c':3,'d':4,'e':5},'f':6,'g':{'h':7,'i':person_a},'j':person_c};

  def test_case1(self):
    data = MixedPrinter(self.a)
    self.assertEqual(data.output, [])

  def test_case2(self):
    data = MixedPrinter(self.b)
    self.assertEqual(data.output, [])

  def test_case3(self):
    data = MixedPrinter(self.c)
    data.output.sort()
    self.assertEqual(data.output, ['a 1\n', 'b 1\n', 'c 1\n'])

  def test_case3(self):
    data = MixedPrinter(self.d)
    data.output.sort()
    self.assertEqual(data.output, ['a 1\n', 'b 1\n', 'c 2\n', 'd 2\n', 'e 2\n', 'f 1\n', 'g 1\n', 'h 2\n'])

  def test_case4(self):
    data = MixedPrinter(self.e)
    data.output.sort()
    self.assertEqual(data.output, ['a 1\n','b 1\n','c 2\n','d 2\n','e 2\n','f 1\n','father 3\n','first_name 3\n','g 1\n','h 2\n','i 2\n','last_name 3\n'])

  def test_case5(self):
    data = MixedPrinter(self.f)
    data.output.sort()
    self.assertEqual(data.output, ['a 1\n','b 1\n','c 2\n','d 2\n','e 2\n','f 1\n','father 2\n','father 3\n','father 3\n','first_name 2\n','first_name 3\n','first_name 3\n','g 1\n','h 2\n','i 2\n','j 1\n','last_name 2\n','last_name 3\n','last_name 3\n'])

if __name__ == '__main__':
    unittest.main()