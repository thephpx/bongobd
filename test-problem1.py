import unittest
from classes.DictPrinter import DictPrinter

class TestDictPrinter(unittest.TestCase):

  def setUp(self):
    self.a = '';
    self.b = None;
    self.c = {'a':1,'b':2,'c':3};
    self.d = {'a':1,'b':{'c':3,'d':4,'e':5},'f':6,'g':{'h':7}};

  def test_case1(self):
    data = DictPrinter(self.a)
    self.assertEqual(data.output, [])

  def test_case2(self):
    data = DictPrinter(self.b)
    self.assertEqual(data.output, [])

  def test_case3(self):
    data = DictPrinter(self.c)
    data.output.sort()
    self.assertEqual(data.output, ['a 1\n', 'b 1\n', 'c 1\n'])

  def test_case3(self):
    data = DictPrinter(self.d)
    data.output.sort()
    self.assertEqual(data.output, ['a 1\n', 'b 1\n', 'c 2\n', 'd 2\n', 'e 2\n', 'f 1\n', 'g 1\n', 'h 2\n'])

if __name__ == '__main__':
    unittest.main()