import unittest
from classes.Node import Node
from classes.Lca import LCA

class TestLeastCommonAccessor(unittest.TestCase):

  def setUp(self):
    self.node1 = Node(1,None)
    self.node2 = Node(2,self.node1)
    self.node3 = Node(3,self.node1)
    self.node4 = Node(4,self.node2)
    self.node5 = Node(5,self.node2)
    self.node6 = Node(6,self.node3)
    self.node7 = Node(7,self.node3)
    self.node8 = Node(8,self.node4)
    self.node9 = Node(9,self.node4)
    self.node10 = None
    self.node11 = None

  def test_case1(self):
    data = LCA(self.node8, self.node9)
    self.assertEqual(data.lca, 4)

  def test_case2(self):
    data = LCA(self.node8, self.node6)
    self.assertEqual(data.lca, 1)

  def test_case3(self):
    data = LCA(self.node8, self.node10)
    self.assertEqual(data.lca, None)

  def test_case3(self):
    data = LCA(self.node10, self.node11)
    self.assertEqual(data.lca, None)

if __name__ == '__main__':
    unittest.main()